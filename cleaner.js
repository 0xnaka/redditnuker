'use strict'
const snoowrap = require('snoowrap')
const Snoostorm = require('snoostorm');
var jsdom = require('jsdom')
const { JSDOM } = jsdom
const { window } = new JSDOM()
const { document } = (new JSDOM('')).window
global.document = document
var $ = require('jquery')(window)
var Uname = "<USERNAME>"
// Build Snoowrap
const r = new snoowrap({
  userAgent: 'Nuker',
  clientId: '<CLIENTID>',
  clientSecret: '<CLIENTSECRET>',
  username: Uname,
  password: '<PASSWORD>'
})
var link = 'https://www.reddit.com/user/' + Uname + '.json'
var commentIDs = [];
var postIDs = [];
$.getJSON(link, function(data) {
  //Push IDs for comments and posts to arrays
  for (var i = 0; i < data.data.children.length; i++) {
    if (data.data.children[i].kind == "t1") {
      commentIDs.push(data.data.children[i].data.id);
    } else {
      postIDs.push(data.data.children[i].data.id);
    }
  }
  //Edit and Delete Array of Comment and Post IDs.
  for (var i = 0; i < commentIDs.length; i++) {
    r.getComment(commentIDs[i]).edit("This Comment Was Removed by The Nuker Script Written by 0xnaka. Click here for source code: https://gitlab.com/0xnaka/redditnuker");
    r.getComment(commentIDs[i]).delete();
    console.log("Comments Deleted");
  }
  for (var i = 0; i < postIDs.length; i++) {
    r.getSubmission(postIDs[i]).edit("This Comment Was Removed by The Nuker Script Written by 0xnaka. Click here for source code: https://gitlab.com/0xnaka/redditnuker");
    r.getSubmission(postIDs[i]).delete
    console.log("Posts Deleted");
  }
})

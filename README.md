# redditnuker

When you delete comments and posts on Reddit they aren't really deleted, they are saved on a publicly accessible archive. 
I wrote this script to allow Reddit users to edit and delete their posts and comments en masse so when the deleted content is sent to the archive the content sent is a standard message. 
This script makes doxxing or even identifying somebody based on deleted content from their Reddit profile near to impossible. For this script to work you must create a personal use script. 
You can do that by going to Reddit’s app preferences and create your own application.
The redirect uri field is also required, but will not be used for this application, so you can put whatever URL you want.
After creating the app, make a note of the app’s client ID and secret ID, as you will need these later on.
In the script replace the <USERNAME>, <PASSWORD>, <CLIENTID>, and <CLIENTSECRET> with their respective data and run it. 
The benefit of deleting content this way is the only party that has access to your Client Secret, Client ID, and Password are you and not a third party deletion app. 
This script is run with nodejs. You can grab that here: https://nodejs.org/en/